﻿using DiplomaGameServer.DAL.Repositories;
using DiplomaGameServer.Entities.Entities;
using DiplomaGameServer.ViewModels.Account;
using System;
using System.Threading.Tasks;

namespace DiplomaGameServer.GL.Services
{
    public class AccountService
    {
        private readonly PlayerRepository _playerRepository;

        public AccountService(string connectionString)
        {
            _playerRepository = new PlayerRepository(connectionString);
        }

        public LoginAccountResponseView Login(LoginAccountView model)
        {
            var result = new LoginAccountResponseView() { ActionNumber = model.ActionNumber };
            var player = _playerRepository.Get(model.Login);
            if(player == null)
            {
                result.Message = "Player is not found!";
                return result;
            }

            if(player.Password != model.Password)
            {
                result.Message = "Password does not match!";
                return result;
            }

            result.PlayerInfo = player.PlayerInfo;
            result.Result = true;
            return result;
        }

        public RegisterAccountResponseView Register(RegisterAccountView model)
        {
            var result = new RegisterAccountResponseView() { ActionNumber = model.ActionNumber };
            var playerWithLogin = _playerRepository.Get(model.Login);

            if(playerWithLogin != null)
            {
                result.Message = "Player with this login already exist!";
                return result;
            }

            var player = new Player
            {
                Id = Guid.NewGuid(),
                Login = model.Login,
                Password = model.Password,
                PlayerInfo = $"{model.Login} info"
            };

            
            var isAdded = _playerRepository.Create(player);
            if(!isAdded)
            {
                result.Message = "Register Error! Try later!";
                return result;
            }

            result.Result = true;
            result.PlayerInfo = player.PlayerInfo;

            return result;
        }
        
    }
}
