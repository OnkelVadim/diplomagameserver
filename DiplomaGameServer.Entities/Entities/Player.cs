﻿using System;

namespace DiplomaGameServer.Entities.Entities
{
    public class Player
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string PlayerInfo { get; set; }
    }
}
