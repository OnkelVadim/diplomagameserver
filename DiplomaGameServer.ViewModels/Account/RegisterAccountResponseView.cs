﻿using System;

namespace DiplomaGameServer.ViewModels.Account
{
    [Serializable]
    public class RegisterAccountResponseView : ResponseBaseView
    {
        public string PlayerInfo;
    }
}
