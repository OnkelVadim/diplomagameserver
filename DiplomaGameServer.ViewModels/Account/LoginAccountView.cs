﻿using System;

namespace DiplomaGameServer.ViewModels.Account
{
    [Serializable]
    public class LoginAccountView : RequestBaseView
    {
        public string Login;
        public string Password;
    }
}
