﻿using System;

namespace DiplomaGameServer.ViewModels.Account
{
    [Serializable]
    public class RegisterAccountView : RequestBaseView
    {
        public string Login;
        public string Password;
        public string ConfirmPassword;
    }
}
