﻿using System;

namespace DiplomaGameServer.ViewModels.Account
{
    [Serializable]
    public class LoginAccountResponseView : ResponseBaseView
    {
        public string PlayerInfo;
    }
}
