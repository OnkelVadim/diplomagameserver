﻿using System;

namespace DiplomaGameServer.ViewModels
{
    [Serializable]
    public class ResponseBaseView
    {
        public int ActionNumber;
        public bool Result;
        public string Message;
    }
}
