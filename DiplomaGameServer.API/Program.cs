﻿using DiplomaGameServer.API.Network;
using System;

namespace DiplomaGameServer.API
{
    class Program
    {
        static void Main(string[] args)
        {
            Server.instance.ServerStart();
            Console.ReadKey();
        }
    }
}
