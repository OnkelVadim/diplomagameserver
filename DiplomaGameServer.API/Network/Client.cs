﻿using System;
using System.Net.Sockets;

namespace DiplomaGameServer.API.Network
{
    class Client
    {
        public int PlayerIndex { get; set; }
        public string IP { get; set; }
        public TcpClient Socket { get; set; }
        public NetworkStream Stream { get; set; }

        private byte[] _readBuff;

        public void Start()
        {
            Socket.SendBufferSize = 4096;
            Socket.ReceiveBufferSize = 4096;
            Stream = Socket.GetStream();
            Array.Resize(ref _readBuff, Socket.ReceiveBufferSize);
            Stream.BeginRead(_readBuff, 0, Socket.ReceiveBufferSize, OnReceiveData, null);
        }

        void CloseConnection()
        {
            Socket.Close();
            Socket = null;
        }

        void OnReceiveData(IAsyncResult result)
        {
            try
            {
                int readBytes = Stream.EndRead(result);
                if (Socket == null)
                {
                    return;
                }
                if (readBytes <= 0)
                {
                    CloseConnection();
                    return;
                }

                byte[] newBytes = null;
                Array.Resize(ref newBytes, readBytes);
                Buffer.BlockCopy(_readBuff, 0, newBytes, 0, readBytes);

                var requestHandler = new RequestHandler(PlayerIndex);
                requestHandler.HandleData(newBytes);

                if (Socket == null)
                {
                    return;
                }

                Stream.BeginRead(_readBuff, 0, Socket.ReceiveBufferSize, OnReceiveData, null);
            }
            catch (Exception ex)
            {
                CloseConnection();
                return;
            }
        }
    }
}
