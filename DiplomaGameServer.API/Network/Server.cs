﻿using System;
using System.Net;
using System.Net.Sockets;

namespace DiplomaGameServer.API.Network
{
    class Server
    {
        public TcpListener ServerSocket;
        public static Server instance = new Server();
        public static Client[] Clients = new Client[100];

        public void ServerStart()
        {
            for (int i = 1; i < 100; i++)
            {
                Clients[i] = new Client();
            }

            ServerSocket = new TcpListener(IPAddress.Any, 5500);
            ServerSocket.Start();
            ServerSocket.BeginAcceptTcpClient(OnClientConnect, null);
            Console.WriteLine("Server has successfully started.");
        }

        void OnClientConnect(IAsyncResult result)
        {
            TcpClient client = ServerSocket.EndAcceptTcpClient(result);
            client.NoDelay = false;
            ServerSocket.BeginAcceptTcpClient(OnClientConnect, null);

            for (int i = 1; i < 100; i++)
            {
                if (Clients[i].Socket == null)
                {
                    Clients[i].Socket = client;
                    Clients[i].PlayerIndex = i;
                    Clients[i].IP = client.Client.RemoteEndPoint.ToString();
                    Clients[i].Start();
                    Console.WriteLine("Incoming Connection from " + Clients[i].IP + "|| Index: " + i);
                    //SendWelcomeMessages
                    return;
                }
            }

        }
    }
}
