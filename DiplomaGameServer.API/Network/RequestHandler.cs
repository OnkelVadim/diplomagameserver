﻿using DiplomaGameServer.ViewModels.Account;
using DiplomaGameServer.GL.Services;
using DiplomaGameServer.ViewModels;
using System.Collections.Generic;
using System.Configuration;
using Newtonsoft.Json;
using System.Text;

namespace DiplomaGameServer.API.Network
{
    class RequestHandler
    {
        private int _index;
        private Dictionary<int, Packet> _packets;

        public RequestHandler(int index)
        {
            _index = index;
            _packets = new Dictionary<int, Packet>();
            _packets.Add(1, HandleRegister);
            _packets.Add(2, HandleLogin);
        }

        public void HandleData(byte[] data)
        {
            var result = new ResponseBaseView();

            try
            {
                var requestString = Encoding.UTF8.GetString(data);
                var requestData = JsonConvert.DeserializeObject<RequestBaseView>(requestString);

                Packet packet;
                if (_packets.TryGetValue(requestData.ActionNumber, out packet))
                {
                    packet.Invoke(requestString);
                    return;
                }

                result.Message = "Invalid request!";
                result.Result = false;

                SendResponse(result);
            }
            catch
            {
                result.Message = "Server error!";
                result.Result = false;

                SendResponse(result);
            }
        }

        void HandleRegister(string jsonData)
        {
            var requestData = JsonConvert.DeserializeObject<RegisterAccountView>(jsonData);
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            var service = new AccountService(connectionString);
            var result = service.Register(requestData);
            SendResponse(result);
        }

        void HandleLogin(string jsonData)
        {
            var requestData = JsonConvert.DeserializeObject<LoginAccountView>(jsonData);
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            var service = new AccountService(connectionString);
            var result = service.Login(requestData);
            SendResponse(result);
        }

        void SendResponse(object data)
        {
            var responseData = JsonConvert.SerializeObject(data);
            var responseBytes = Encoding.UTF8.GetBytes(responseData);
            Server.Clients[_index].Stream.BeginWrite(responseBytes, 0, responseBytes.Length, null, null);
        }

        private delegate void Packet(string jsonData);
    }
}
