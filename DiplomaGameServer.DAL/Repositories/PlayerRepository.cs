﻿using System;
using System.Data.SqlClient;
using System.Threading.Tasks;
using DiplomaGameServer.Entities.Entities;

namespace DiplomaGameServer.DAL.Repositories
{
    public class PlayerRepository
    {
        private readonly string _connectionString;

        public PlayerRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public bool Create(Player player)
        {
            var sql = @"INSERT INTO Players (Id, Login, Password, PlayerInfo) VALUES (@Id, @Login, @Password, @PlayerInfo)";
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var command = new SqlCommand(sql, connection);
                command.Parameters.Add(new SqlParameter("@Id", player.Id));
                command.Parameters.Add(new SqlParameter("@Login", player.Login));
                command.Parameters.Add(new SqlParameter("@Password", player.Password));
                command.Parameters.Add(new SqlParameter("@PlayerInfo", player.PlayerInfo));
                var result = command.ExecuteNonQuery();
                return result == 1 ? true : false;
            }
        }

        public Player Get(string login)
        {
            string sqlExpression = "SELECT * FROM Players WHERE Login = @Login";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.Add(new SqlParameter("@Login", login));
                SqlDataReader reader = command.ExecuteReader();

                if (!reader.HasRows)
                {
                    return null;
                }

                var result = new Player();

                while (reader.Read()) // построчно считываем данные
                {
                    result.Id = reader.GetGuid(0);
                    result.Login = reader.GetString(1);
                    result.Password = reader.GetString(2);
                    result.PlayerInfo = reader.GetString(3);
                }

                reader.Close();

                return result;
            }
        }
    }
}
